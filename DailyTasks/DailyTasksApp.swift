//
//  DailyTasksApp.swift
//  DailyTasks
//
//  Created by Michael Shepherd on 08/02/2021.
//

import SwiftUI

@main
struct DailyTasksApp: App {
    @StateObject private var navigationButtonModel = NavigationButtonModel()
    @StateObject private var taskData = TaskData()
    
    @UIApplicationDelegateAdaptor(TaskAppDelegate.self) var appDelegate
    
    var body: some Scene {
        WindowGroup {
            ContentView(taskAppDelegate: appDelegate).environmentObject(taskData).environmentObject(navigationButtonModel)
        }
    }
}
