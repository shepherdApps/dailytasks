//
//  ContentView.swift
//  DailyTasks
//
//  Created by Michael Shepherd on 08/02/2021.
//
//  Displays the list of tasks and allows creation of new tasks
//

import SwiftUI
import UserNotifications

class TaskAppDelegate: NSObject, UIApplicationDelegate {
    private var taskData : TaskData? = nil
    
    private var taskNotificationDelegate: TaskNotificationDelegate
    
    override init() {
        self.taskNotificationDelegate = TaskNotificationDelegate()
        super.init()
        requestNotificationAuthorization()
    }
    
    func requestNotificationAuthorization() {
        let notificationHandler = UNUserNotificationCenter.current()
        notificationHandler.requestAuthorization(options: [.alert, .badge]) { [weak self] granted, error in
            if let error = error {
                print(error.localizedDescription)
            }
            
            self?.taskNotificationDelegate.setNotificationsEnabled(notificationsEnabled: granted)
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        taskNotificationDelegate.setAsDelegate()
        return true
    }
    
    func getNotificationsEnabled() -> Bool {
        return taskNotificationDelegate.getNotificationsEnabled()
    }
    
    func setTaskData(taskData: TaskData) {
        self.taskData = taskData
        taskNotificationDelegate.setTaskData(taskData: taskData)
    }
    
    func hasTaskDataBeenSet() -> Bool {
        return taskData == nil
    }
}

struct ContentView: View {
    @EnvironmentObject var taskData: TaskData
    @EnvironmentObject var navigationButtonModel: NavigationButtonModel
    
    @ObservedObject var eventHandler = AppEventHandler()
    
    var taskAppDelegate: TaskAppDelegate
    
    init(taskAppDelegate: TaskAppDelegate) {
        self.taskAppDelegate = taskAppDelegate
    }
    
    func setTaskDataForDelegate() {
        taskAppDelegate.setTaskData(taskData: taskData)
    }
    
    func displayTasksForList(taskList: [Task], headerText: Text) -> some View {
        return Group {
            if taskList.count > 0 {
                Section(header: headerText) {
                    ForEach(taskList) { task in
                        NavigationLink(destination: TaskDetailsView(task: task)) {
                            TaskView(task: task).padding(.horizontal, 10)
                        }
                    }
                }
            } else {
                EmptyView()
            }
        }
    }
    
    var body: some View {
        NavigationView {
            VStack {
                if(taskData.getTasks().count > 0) {
                    List {
                        displayTasksForList(taskList: taskData.getOngoingTasks(), headerText: Text("In Progress"))
                        displayTasksForList(taskList: taskData.getOverdueTasks(), headerText: Text("Overdue"))
                    }.listStyle(PlainListStyle())
                }
                else {
                    Text("You have no tasks currently ongoing")
                }
            }
            .padding(.top, 20)
            .navigationBarTitle("Daily Tasks")
            .toolbar(content: {
                ToolbarItem(placement: .navigationBarLeading) {
                    NavigationLink(destination: SettingsView(taskAppDelegate: taskAppDelegate), isActive: $navigationButtonModel.pushedForSettings) {
                            Image(systemName: "gearshape").foregroundColor(.blue)
                    }
                }
                ToolbarItem(placement: .navigationBarTrailing) {
                    NavigationLink("Add Task", destination: NewTaskView(), isActive: $navigationButtonModel.pushedForNewTask).font(.title2)
                }
            })
            .onAppear(){
                taskAppDelegate.setTaskData(taskData: taskData)
            }
        }
        .onReceive(self.eventHandler.$enteredForeground) { _ in
            removeBadgeNotifications()
        }
    }

}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(taskAppDelegate: TaskAppDelegate()).environmentObject(TaskData()).environmentObject(NavigationButtonModel())
    }
}
