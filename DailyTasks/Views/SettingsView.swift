//
//  SettingsView.swift
//  DailyTasks
//
//  Created by Michael Shepherd on 13/02/2021.
//
//  Controls and displays all the settings for the App

import SwiftUI

enum NotificationLevel: String, CaseIterable {
    case All, Important, None, Disabled
}

struct SettingsView: View {
    @Environment(\.presentationMode) var presentationMode
    
    @EnvironmentObject var navigationButtonModel: NavigationButtonModel
    
    @State var selectedNotificationLevel: NotificationLevel
    @State var notificationsEnabled: Bool
    
    var taskAppDelegate: TaskAppDelegate
    
    init(taskAppDelegate: TaskAppDelegate) {
        self.taskAppDelegate = taskAppDelegate
        if UserDefaults.standard.value(forKey: "NotificationLevel") == nil {
            UserDefaults.standard.set(NotificationLevel.All.rawValue, forKey: "NotificationLevel")
        }
        let notificationLevel = NotificationLevel(rawValue: UserDefaults.standard.value(forKey: "NotificationLevel") as! String)!
        
        _selectedNotificationLevel = State(initialValue: notificationLevel)
        _notificationsEnabled = State(initialValue: self.taskAppDelegate.getNotificationsEnabled())
        
        if !notificationsEnabled {
            _selectedNotificationLevel = State(initialValue: .Disabled)
        }
    }
    
    var body: some View {
        Form {
            Section {
                Picker("Notification Level", selection: $selectedNotificationLevel) {
                    ForEach(NotificationLevel.allCases, id: \.self) {
                        if !(notificationsEnabled && $0.rawValue == "Disabled") {
                            Text($0.rawValue)
                        }
                    }
                }.disabled(!notificationsEnabled)
            }
            
            Section {
                Button(action: {
                    UserDefaults.standard.set(selectedNotificationLevel.rawValue, forKey: "NotificationLevel")
                    navigationButtonModel.pushedForSettings = false
                    presentationMode.wrappedValue.dismiss()
                }, label: {
                    Text("Confirm Changes")
                })
            }
        }
        .navigationTitle("Settings")
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView(taskAppDelegate: TaskAppDelegate()).environmentObject(NavigationButtonModel())
    }
}
