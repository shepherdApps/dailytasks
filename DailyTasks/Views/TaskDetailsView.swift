//
//  TaskDetailsView.swift
//  DailyTasks
//
//  Created by Michael Shepherd on 11/02/2021.
//
//  View to display all the details about a selected task and complete the task once finished
//

import SwiftUI

struct TaskDetailsView: View {
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var taskData: TaskData
    
    private let task: Task
    private let dateFormatter = DateFormatter()
    
    init(task: Task) {
        self.task = task
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
    }
    
    var body: some View {
        VStack {
            HStack {
                Text("Task Name: ").bold()
                Spacer()
                Text(task.id)
            }.padding(EdgeInsets(top: 15, leading: 15, bottom: 15, trailing: 15))
            
            HStack {
                let importantString = task.important ? "Yes": "No"
                Text("Important: ").bold()
                Spacer()
                Text(importantString)
            }.padding(EdgeInsets(top: 15, leading: 15, bottom: 15, trailing: 15))
            
            HStack {
                Text("Description: ").bold()
                Spacer()
            }.padding(EdgeInsets(top: 15, leading: 15, bottom: 0, trailing: 15))
            
            HStack {
                let description = task.description == "" ? "No Description Given": task.description
                Text(description)
                Spacer()
            }.padding(EdgeInsets(top: 0, leading: 15, bottom: 15, trailing: 15))
            
            HStack {
                let dateString = dateFormatter.string(from: task.completeBefore)
                Text("Complete Before: ").bold()
                Spacer()
                Text(dateString)
            }.padding(EdgeInsets(top: 15, leading: 15, bottom: 15, trailing: 15))
            
            Button("Complete Task", action: {
                taskData.removeTask(taskToRemove: task)
                removeNotificationsForTask(task: task)
                presentationMode.wrappedValue.dismiss()
            }).padding(EdgeInsets(top: 15, leading: 15, bottom: 15, trailing: 15))
            
            Spacer()
        }.padding(.horizontal, 20).padding(.vertical, 20)
        .navigationBarTitle("Task Details")
    }
}

struct TaskDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        TaskDetailsView(task: Task(id: "Test", description: "My Task", important: false, completeBefore: Date())).environmentObject(TaskData())
    }
}
