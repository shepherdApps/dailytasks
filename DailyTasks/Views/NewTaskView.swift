//
//  NewTaskView.swift
//  DailyTasks
//
//  Created by Michael Shepherd on 09/02/2021.
//
//  View for creating a new task and adding to the list of tasks
//

import SwiftUI
import UserNotifications

struct NewTaskView: View {
    @Environment(\.presentationMode) var presentationMode
    @EnvironmentObject var taskData: TaskData
    @EnvironmentObject var navigationButtonModel: NavigationButtonModel
    
    @State private var inputTaskName = ""
    @State private var important = false
    @State private var description = ""
    @State private var completeBefore = Date().addingTimeInterval(3600)
    
    @State private var validationMessage = ""
    @State private var showValidationAlert = false
    
    // Check that the entered inputs are valid
    private func checkValid() -> Bool {
        let now = Date()
        
        if inputTaskName.count > 20 || inputTaskName.count == 0 {
            validationMessage = "Task Name must contain between 1 and 20 characters"
            return false
        } else if now >= completeBefore {
            validationMessage = "Task completion time must be set in the future"
            return false
        } else {
            let matchingName = taskData.getTasks().filter { $0.id == inputTaskName }
            if matchingName.count > 0 {
                validationMessage = "Task name has already been used"
                return false;
            }
        }
        
        return true
    }
    
    var body: some View {
        Form {
            Section(header: Text("Task Details")) {
                TextField("Task Name", text: $inputTaskName)
                    
                Toggle(isOn: $important, label: {
                    Text("Important")
                })
            }
                    
            Section(header: Text("Task Description")) {
                TextEditor(text: $description)
            }
                    
            Section(header: Text("Complete By: ")) {
                DatePicker("Complete By:", selection: $completeBefore).datePickerStyle(WheelDatePickerStyle())
            }
            
            Section {
                Button(action: {
                    if checkValid() {
                        let newTask = Task(id: inputTaskName, description: description,
                                                   important: important, completeBefore: completeBefore)
                        taskData.addTask(newTask: newTask)
                        navigationButtonModel.pushedForNewTask = false
                        taskData.save()
                        if UserDefaults.standard.value(forKey: "NotificationLevel") == nil {
                            UserDefaults.standard.set(NotificationLevel.All.rawValue, forKey: "NotificationLevel")
                        }
                        let notificationLevel = NotificationLevel(rawValue: UserDefaults.standard.value(forKey: "NotificationLevel") as! String)!
                        setupTaskNotifications(task: newTask, notificationLevel: notificationLevel)
                        presentationMode.wrappedValue.dismiss()
                    } else {
                        showValidationAlert = true
                    }
                }, label: {
                    Text("Create Task")
                }).alert(isPresented: $showValidationAlert, content: {
                    Alert(title: Text("Invalid Task"), message: Text(validationMessage), dismissButton: .cancel())
                })
            }
        }.navigationBarTitle("Create New Task")
    }
}

struct NewTaskView_Previews: PreviewProvider {
    static var previews: some View {
        NewTaskView().environmentObject(TaskData()).environmentObject(NavigationButtonModel())
    }
}
