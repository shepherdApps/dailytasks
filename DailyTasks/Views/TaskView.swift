//
//  TaskView.swift
//  DailyTasks
//
//  Created by Michael Shepherd on 08/02/2021.
//
//  View containing structure of a single row of the Tasks list
//

import SwiftUI

struct TaskView: View {
    var task: Task
    
    private let dateFormatter = DateFormatter()
    
    init(task: Task) {
        self.task = task
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
    }
    
    var body: some View {
        HStack {
            VStack(alignment: .leading) {
                Spacer()
                Text(task.id)
                Spacer()
                let dateString = dateFormatter.string(from: task.completeBefore)
                Text("Due: " + dateString)
                Spacer()
            }
            Spacer()
            
            if task.important {
                Image(systemName: "exclamationmark.circle.fill").foregroundColor(.blue).font(.system(size: 25))
            }
        }
    }
}

struct TaskView_Previews: PreviewProvider {
    static var previews: some View {
        TaskView(task: Task(id: "MyTask", description: "Test", important: true, completeBefore: Date()))
            .previewLayout(.fixed(width: 300, height: 70))
    }
}
