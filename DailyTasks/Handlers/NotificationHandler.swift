//
//  NotificationHandler.swift
//  DailyTasks
//
//  Created by Michael Shepherd on 13/02/2021.
//
//  Handles the creation and removal of all notifications in the app
//

import Foundation
import UserNotifications
import UIKit

let hourBeforeMessage = "You only have 1 hour left to complete this task"
let overdueMessage = "This task is now overdue"

func setupNotification(notificationHandler: UNUserNotificationCenter, task: Task, timeToPerform: Date, message: String, idPrefix: String) {
    let content = UNMutableNotificationContent()
    content.title = "Task: " + task.id
    content.body = message
    content.badge = 1
    
    let triggerTime = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute], from: timeToPerform)
    let trigger = UNCalendarNotificationTrigger(dateMatching: triggerTime, repeats: false)
    
    let dateFormat = DateFormatter()
    dateFormat.dateStyle = .short
    dateFormat.timeStyle = .short
    
    let request = UNNotificationRequest(identifier: idPrefix + task.id, content: content, trigger: trigger)
    
    notificationHandler.add(request) { (error) in
        if error != nil {
            print(error!.localizedDescription)
        }
    }
}

func removeNotificationsForTask(task: Task) {
    let notificationHandler = UNUserNotificationCenter.current()
    notificationHandler.removePendingNotificationRequests(withIdentifiers: ["hourBefore" + task.id, "overdue" + task.id])
}

func setupTaskNotifications(task: Task, notificationLevel: NotificationLevel) {
    guard notificationLevel != .None && !(notificationLevel == .Important && !task.important) else { return }
    
    let notificationHandler = UNUserNotificationCenter.current()
    
    let hourBefore = Calendar.current.date(byAdding: .hour, value: -1, to: task.completeBefore)!
    let minuteAfter = Calendar.current.date(byAdding: .minute, value: 1, to: task.completeBefore)!
    setupNotification(notificationHandler: notificationHandler, task: task, timeToPerform: hourBefore, message: hourBeforeMessage, idPrefix: "hourBefore")
    setupNotification(notificationHandler: notificationHandler, task: task, timeToPerform: minuteAfter, message: overdueMessage, idPrefix: "overdue")
}

func removeBadgeNotifications() {
    UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { settings in
        if settings.authorizationStatus == .authorized {
            UserDefaults.standard.set(0, forKey: "badgeCount")
            
            DispatchQueue.main.async {
                UIApplication.shared.applicationIconBadgeNumber = 0
            }
        }
    })
}

class TaskNotificationDelegate: NSObject, UNUserNotificationCenterDelegate {
    
    private var taskData: TaskData = TaskData()
    
    private var notificationsEnabled: Bool = false
    
    override init() {
        super.init()
        UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { [weak self] settings in
            self?.notificationsEnabled = settings.authorizationStatus == .authorized
        })
    }
    
    func setTaskData(taskData: TaskData) {
        self.taskData = taskData
    }
    
    func setAsDelegate() {
        UNUserNotificationCenter.current().delegate = self
    }
    
    func getNotificationsEnabled() -> Bool {
        return notificationsEnabled
    }
    
    func setNotificationsEnabled(notificationsEnabled: Bool) {
        self.notificationsEnabled = notificationsEnabled
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        if notification.request.content.body == overdueMessage {
            let notificationTitle = notification.request.content.title
            
            let prefixIndex = notificationTitle.index(notificationTitle.firstIndex(of: " ")!, offsetBy: 1)
            let range = prefixIndex...(notificationTitle.index(notificationTitle.endIndex, offsetBy: -1))
            let taskId = String(notificationTitle[range])
            
            let task = taskData.getTaskById(taskId: taskId)!
            
            taskData.removeTask(taskToRemove: task)
            taskData.addTask(newTask: task)
        }
        
        completionHandler(UNNotificationPresentationOptions(rawValue: 0))
    }
}
