//
//  AppEventHandler.swift
//  DailyTasks
//
//  Created by Michael Shepherd on 13/02/2021.
//

import Foundation
import UIKit

class AppEventHandler: ObservableObject {
    
    @Published var enteredForeground = true
    
    init() {
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIScene.willEnterForegroundNotification, object: nil)
    }
    
    @objc func willEnterForeground() {
        enteredForeground.toggle()
    }
}
