//
//  Task.swift
//  DailyTasks
//
//  Created by Michael Shepherd on 08/02/2021.
//
//  Task Object containing all the information about a task
//

import Foundation
import Combine

class TaskData: ObservableObject {
    static let saveKey = "TaskList"
    
    @Published private var tasks: [Task]
    
    init() {
        if let data = UserDefaults.standard.data(forKey: Self.saveKey) {
            if let decoded = try? JSONDecoder().decode([Task].self, from: data) {
                self.tasks = decoded
                return
            }
        }
        
        tasks = []
    }
    
    func save() {
        if let encoded = try? JSONEncoder().encode(tasks) {
            UserDefaults.standard.set(encoded, forKey: Self.saveKey)
        }
    }
    
    func addTask(newTask: Task) {
        tasks.append(newTask)
        save()
    }
    
    func removeTask(taskToRemove: Task) {
        if let index = tasks.firstIndex(of: taskToRemove) {
            tasks.remove(at: index)
            save()
        }
    }
    
    func getTasks() -> [Task] {
        return tasks
    }
    
    func getTaskById(taskId: String) -> Task? {
        for task in tasks {
            if task.id == taskId {
                return task
            }
        }
        
        return nil
    }
    
    func getOngoingTasks() -> [Task] {
        let now = Date()
        return tasks.filter { $0.completeBefore >= now }
    }
    
    func getOverdueTasks() -> [Task] {
        let now = Date()
        return tasks.filter { $0.completeBefore < now }
    }
}

class NavigationButtonModel: ObservableObject {
    @Published var pushedForNewTask = false
    @Published var pushedForSettings = false
    @Published var pushedForTaskDetails = false
}

struct Task: Identifiable, Codable, Equatable {
    var id: String
    var description: String
    var important: Bool
    var completeBefore: Date
    
    var notificationId: String
    
    init(id: String, description: String, important: Bool, completeBefore: Date) {
        self.id = id
        self.description = description
        self.important = important
        self.completeBefore = completeBefore
        self.notificationId = UUID().uuidString
    }
}
